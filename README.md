## Contexte du projet

L'objectif est de concevoir une application permettant aux employés d'une grande entreprise de commander du matériel.
Pour ceci, nous avons tout d'abord analysé et ciblé les besoins utilisateur.
Ensuite, nous avons conçu un diagramme de cas d'utilisations. Ensuite, nous avons conçu un diagramme de classe afin de mieux déterminer quelles sont les classes qui devront être créé et développées. 
Enfin, afin d'avoir une base de donnée propre, nous avons adapté le diagramme de classe en diagramme de modèle pour déterminer quelles tables nous allons devoir créer.
---
## Analyse des besoins
**QUI ?**
* Client : l'Entreprise ayant demandé la conception de l'app
* Utilisateurs :
    * Salariés
    * Chefs de service
    * Employé du service d'achats
    * Employés du service de comptabilité
    * Employés de la Direction générale

**USAGE ?**
* Usage professionnel en entreprise.

**QUOI ?**
* obligatoire :
    * Un salarié doit pouvoir déclencher une commande de matériel ou de logiciel.
    * Un chef de service peut valider ou refuser la commande.
    * Un chef de service peut consulter l'historique des dépenses de son service.
    * Un employé du service achats peut procéder au paiement de la commande.
    * Le service comptabilité et la direction générale peuvent afficher l'historique des recettes et des dépenses et leur catégorie.
* facultative :
    * Un chef de service qui peut visualiser son budget prévisionnel et restant.
    * Un chef de service qui peut réapprovisionner son budget annuellement.
    * Un employé du service achats peut consulter la liste des factures en attente.

**COMMENT ?**
* contexte final : Application Web.
* technologies : Springboot / Angular / MySQL.
* Délais : base de 6 mois ajustable.


---
##### Diagramme de cas d'utilisation :

Nous avons déterminé qu'il y auraient 5 acteurs au sein de l'application, qui seraient tous des salariés.
* un Employé d'un secteur, qui peut déclencher une commande de matériel ou de logiciel.
* Un chef de service, qui peut :
    * valider ou refuser une commande.
    * Visualiser son budget prévisionnel.
    * Visualiser son budget restant.
    * Réapprovisionner son budget annuellement.
    * Consulter l'historique des dépenses.
* Un employé du service d'achats, qui peut : 
    * procéder au paiement d'une commande.
    * consulter la liste des factures en attente.
* Un employé du service de comptabilité qui peut afficher l'historique des recettes et des dépenses et leur catégorie.
* Un employé de la direction générale qui peut afficher l'historique des recettes et des dépenses et leur catégorie.

Nous avons aussi constaté qu'il y a 4 sujets distincts qui sont : 
* Les commandes
* Les factures
* Le budget
* L'historique

--- 
##### Diagramme de classe :
Ensuite, nous appuyants sur le diagramme de cas d'utilisations, nous avons déterminé les différentes classes qui allaient devoir être développées. Nous avons essayé d'être le plus simple possible en précisant les **controllers, entités et repository** pour chaque classe.
Les repository contiennent les différentes méthodes qui seront implémentées et aussi utilisées dans les controllers, et ils contiennent aussi toutes les méthodes du **CRUD**.
Les entités seront les models qui contiendrons toutes les propriétés des objets qui seront utilisés.
--- 
##### Diagramme de modèle :
Enfin, après avoir déterminés nos différentes classes et leurs relations, nous avons repris le diagramme pour l'alléger et en faire un diagramme de modèle afin de représenter les différentes tables qui seront dans la base de données.
Nous avons identifié 5 tables différentes. :
* **Service**, en *one to many* avec la table **Employee**.
* **Employee**, en *many to one* avec la table **Service**, et en *one to many* avec la table **Order**.
* **Order**, en *many to many* avec la table **Element**, *many to many* avec la table **Category**, et en *many to one* avec la table **Employee**.
